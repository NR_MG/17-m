#include <iostream>
#include <string>

class Mine
{
private:
	int a, b, c;
public:
	Mine ()
	{}

	void GetData()
	{
		std::cout << "a = " << a << "\n";
		std::cout << "b = " << b << "\n";
		std::cout << "c = " << c << "\n";
	}

	void SetData(int newA, int newB, int newC)
	{
		a = newA;
		b = newB;
		c = newC;
	}
};

class Vector
{
public:
	Vector() : x(0), y(0), z(0)
	{}
	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}
	void Show()
	{
		std::cout << "\n" << x << ' ' << y << ' ' << z << "\n";
	}
	void Length()
	{
		float lenght = sqrt(x * x + y * y + z * z);
		std::cout << lenght << "\n";
	}
private:
	double x;
	double y;
	double z;
};

int main()
{
	Mine example1;
	example1.SetData(10, 20, 30);
	example1.GetData();

	Vector v(10, 20, 35);
	v.Show();
	v.Length();

	return 0;
}